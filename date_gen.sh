#! /bin/zsh

date -u +"%Y-%m-%dT%H:%M:%SZ" | tr -d '\n' | pbcopy
