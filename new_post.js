#!/usr/bin/env node

const [,, ...args] = process.argv

console.log(`Creating post with arguments: ${args}`)

const fs = require('fs')

const buildPath = (args) => {
  const prefix = './content/blog'
  if (args.length > 1) {
    return `${prefix}/${joinedArgs(args)}`
  } else {
    return `${prefix}/${args[0]}`
  }
}

const lowerCaseArgs = (args) => {
  return args.map(arg => arg.toLocaleLowerCase())
}

const joinedArgs = (args, separator="-") => {
  return args.join(separator)
}

path = buildPath(lowerCaseArgs(args))
fs.mkdirSync(path)
file = `${path}/${joinedArgs(lowerCaseArgs(args))}.md`
fs.closeSync(fs.openSync(file, 'w'));

const frontMatter =
`---
title: ${joinedArgs(args, " ")}
date: '${new Date().toISOString()}'
type: post
published: false
---`

fs.appendFile(file, frontMatter, function (err) {
  if (err) throw err;
  console.log('Saved!');
});
