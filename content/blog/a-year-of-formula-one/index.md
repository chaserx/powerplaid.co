---
title: A Year of Formula One
date: '2019-12-01T18:16:23Z'
type: post
published: true
---

![Photo by Darren Nunis on Unsplash](./darren-nunis-DcPyI0LFgAA-unsplash.jpg)

Today marks the end of an experiment. I watched an entire season of Formula 1 racing. It's weird. I've never been into racing. My family isn't into racing. But, after listening to CGP Grey and Brady Haran talk about it on some recent Hello Internet episodes, [#124](http://www.hellointernet.fm/podcast/124) and [#127](http://www.hellointernet.fm/podcast/127), I thought I'd give it a go.

Walking in completely unaware of what's happening would have been a bad idea. On their advice I watched the [Formula 1: Drive to Survive on Netflix](https://www.netflix.com/title/80204890) and was surprised at how much I liked it. The creators of that show really stoked the drama of the sport. Drama between a extremely competitive group of drivers both across and within teams.

From there, I watched a similar show [Grand Prix Driver on Amazon Prime Video](https://www.amazon.com/gp/video/detail/B078WFL2TW/ref=atv_wl_hom_c_unkc_1_8). Not as good, but more information and some behind the scenes of what it takes to be a driver.

As if that weren't enough, I was then motivated to watch the movie [Senna](https://www.amazon.com/Senna-Ayrton/dp/B006MGDVFM/ref=sr_1_1?keywords=Senna&qid=1575219088&s=instant-video&sr=1-1) to gather even more history and backstory.

After all that, I wasn't deterred.

So, here are my impressions from watching the 2019 season of F1.

* With broadcasts on ESPN in the USA, most of the time they are presented commercial free. This is so refreshing compared to say watching American Football that's full of commercials and therefore lasts like 3 hours. In contrast, watching an F1 race is like 1.5 hours. Oh, also, like every other week. An hour and a half every other week or so, is easy to commit too.
* Some kind of DVR is super useful because timezones are difficult.
* I had no idea that the teams construct a pair of new cars and spare parts from scratch each season. That's pretty incredible.
* I'm not exactly sure what each of the 20 people in a the pit lane do, but to see them work in unison to swap a set of tires in under 2 seconds is very impressive to [watch](https://www.youtube.com/watch?v=3ZVf6Sc989E).
* There's a mountain of telemetry data that streams off of each car. Thinking about how that's achieved technically is a pretty good mental exercise. How would you get the data, store it, visualize it, what do you do with it later. It's no wonder that the data scientists and strategists for these teams take practice data and run thousands of simulations with that track performance data to predict the outcome of the race and how best approach a race strategy. Of course strategy can get thrown out when you put a driver in the seat.
* The distribution of race winners is very lopsided. I would much prefer to have a more level playing field between drivers than a few elite teams with mega budgets who win again and again and again. Not to discredit Lewis Hamilton and Mercedes - what they have achieved is impressive. However, I find myself rooting for those chasing them.
* Similarly, the best of the rest is an interesting and dynamic place to watch. Drivers and Teams who rarely get on the podium, but who are fighting to stay inside the points (the top 10 spots).
* I feel bad for Williams Racing. They have a long and storied history of racing, but seem to be really struggling. It seems that even though you are using a Mercedes engine that it's not just about the power unit.
* Sometimes it can be an anti-climactic result. Same winners who lead the race the entire time end up winning.
* I think I like [Monaco Grand Prix](https://en.wikipedia.org/wiki/Monaco_Grand_Prix) the best with it's tight, in city track.

Overall, it's been a pretty enjoyable experience. I will probably watch again next season. [F1 2020](https://en.wikipedia.org/wiki/2020_Formula_One_World_Championship) starts again in March.

***

image credit: Photo by Darren Nunis on Unsplash
