---
title: One More Time
date: '2019-11-30T14:57:55Z'
type: post
published: true
---


![One More Time](./one_more_time.jpg)

One 👏 More 👏 Time 👏

Ok. I'm going to try the blog thing one more time. This time, however, I'm going to try to do it
for me, just me. I already write things down in a journal, so why not here in public where appropriate.

I'm looking to get something out of it.

* Collect or archive my thoughts
* Actively do something with the plethora of tabs that I'm too anxious to close for fear of forgetting.
* Flex the writing muscles
* Push myself to be less concerned with the details - don't get so caught up that you never start
* Try out new things


In my notebooks I start a new notebook with "the first page is so enlightening...the rest is just useless scribblings." Seems good to do the same here.

We'll see how this goes.

***

image credit: https://www.facebook.com/onemoretimeofficial/
